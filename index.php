<?php

require_once 'vendor/autoload.php';

function getRequestExample()
{
    // Creating API client
    $apiClient = \Rauc\ApiClientFactory::create();

    // Setting API base URL
    $apiClient->setBaseUrl('https://postman-echo.com');

    // Client returns responses compliant with PSR-7
    // Calling the following method will return object which implements \Psr\Http\Message\ResponseInterface
    // Client may throw \Rauc\ApiClientException
    $response = $apiClient->get('/get?foo1=bar1&foo2=bar2');

    $body = (string) $response->getBody();
    $parsedBody = json_decode($body, true);

    echo $parsedBody['args']['foo1'];
}

function postRequestExample()
{
    // Creating API client
    $apiClient = \Rauc\ApiClientFactory::create();

    // Setting API base URL
    $apiClient->setBaseUrl('https://postman-echo.com');

    // Client returns responses compliant with PSR-7
    // Calling the following method will return object which implements \Psr\Http\Message\ResponseInterface
    // Client may throw \Rauc\ApiClientException
    $response = $apiClient->post('/post', ['foo' => 'bar']);

    echo $response->getStatusCode();
}

function basicAuthenticationExample()
{
    // Creating API client
    $apiClient = \Rauc\ApiClientFactory::create();

    //  Setting API base URL
    $apiClient->setBaseUrl('https://postman-echo.com');

    // Setting authentication method
    $basic = new \Rauc\Authentication\BasicAuthentication();

    $basic->setUsername('postman')
        ->setPassword('password');

    $apiClient->setAuthMethod($basic);

    // Client returns responses compliant with PSR-7
    // Calling the following method will return object which implements \Psr\Http\Message\ResponseInterface
    // Client may throw \Rauc\ApiClientException
    $response = $apiClient->get('/basic-auth');

    if ($response->getStatusCode() === 200) {
        echo 'Authenticated';
    } else {
        echo 'Authentication failed';
    }
}

getRequestExample(); // bar1
echo '<br>';
postRequestExample(); // 200
echo '<br>';
basicAuthenticationExample(); // Authenticated
